## Ubercart KindLink module (Drupal 6)

This is module for Drupal 6 ubercart.

## Installation Instructions

### Dependencies
This module depends on the following modules:

* Payment
* Conditional Actions
* Order
* Store
* Token

### Installation
How to install this module:

1. Place all module files in 
```ubercart/payment/uc_kindlink```
1. Install the module from Drupal6 Administration 
```Administer -> Site building -> Modules```

**Note:** if you don't see the module you need to clear Drupal's cache  ```Administer -> Site configuration -> Performance```


## Configuration

1. Configure your Gateway from ```Administer -> Store administration -> Configuration -> Payment settings```
1. There you navigate to ```Edit -> Payment gateways -> Kindlink Payment Gateway settings```
1. Configure your Payment methods from ```Administer -> Store administration -> Configuration -> Payment settings```
1. There you navigate to ```Edit -> Payment methods -> Kindlink Payment Method Title settings```